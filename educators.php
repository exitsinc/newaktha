<?php include('includes/main_header.php'); ?>
<div class="ftco-blocks-cover-1">
  
  <div class="site-section-cover overlay">
    <div class="container">
      <div class="row align-items-center ">
        <div class="col-md-5 mt-5 pt-5">
          <span class="text-cursive h5 text-red">حياكم الله في موقع نواخذه ....</span>
          <h1 class="mb-3 font-weight-bold text-teal">Bring Fun Life To Your Kids</h1>
          <p>Amazing Playground for your kids</p>
          <p class="mt-5"><a href="#" class="btn btn-primary py-4 btn-custom-1">Learn More</a></p>
        </div>
        <div class="col-md-6 ml-auto align-self-end">
          <img src="images/kid_transparent.png" alt="Image" class="img-fluid">
          <!-- <img src="images/WhatsApp Image 2020-07-27 at 10.33.11.jpeg" alt="Image" class="img-fluid"> -->
          
        </div>
      </div>
    </div>
  </div>
</div>
<!--     <div class="site-section">
  <div class="container">
    <div class="row">
      <div class="col-lg-4">
        <div class="block-2 red">
          <span class="wrap-icon">
            <span class="icon-home"></span>
          </span>
          <h2>Indoor Games</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima nesciunt, mollitia, hic enim id culpa.</p>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="block-2 yellow">
          <span class="wrap-icon">
            <span class="icon-person"></span>
          </span>
          <h2>Outdoor Game And Event</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima nesciunt, mollitia, hic enim id culpa.</p>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="block-2 teal">
          <span class="wrap-icon">
            <span class="icon-cog"></span>
          </span>
          <h2>Camping for Kids</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima nesciunt, mollitia, hic enim id culpa.</p>
        </div>
      </div>
    </div>
  </div>
</div> -->
<div class="site-section bg-light">
  <div class="container">
    <div class="row">
      <div class="col-md-6 mb-5 mb-md-0">
        <img src="images/img_1.jpg" alt="Image" class="img-fluid">
      </div>
      <div class="col-md-6 ml-auto pl-md-6">
        <span class="text-cursive h5 text-red">مربي
        </span>
        <!-- <h3 class="text-black">Bring Fun Life To Your Kids</h3> -->
        <ul>
          <li><p><span>&nbsp;&nbsp;نواخذه هي مسابقة تعليمية تستهدف تعريف الابناء بمسيرة الاباء وانجازاتهم في منطقة الخليج العربي والمسابقة تحتوي على 125 سؤالا عن شخصيات خليجية في الرياضة والادب والفن والاعلام والثقافة والادارة منهم من رحل عنا فلهم الرحمة والغفران .. والبعض  بيننا ندعو لهم بالصحة والعافية وان يطيل الله في اعمارهم ليستمروا في العطاء ...  </span></p></li>
          <li><p><span>&nbsp;&nbsp;نالمسابقة مكونة من 5 مستويات في كل مستوى 10 اسئلة وللانتقال من مستوى الى آخر يجب الاجابة على 6 او 7 او 8 اسئلة بشكل صحيح والعدد يختلف حسب المستوى، ولكن في حالة عدم اجتياز اي مستوى يمكن اعادة المحاولة وستكون لديه اسئلة جديدة ومختلفة ولكل مستوى محاولتين او ثلاث محاولات .</span></p></li>
          <li><p><span>&nbsp;&nbsp;نامع كل سؤال هناك الاجابة الصحيحة وهي عبارة عن فقرة او فقرتين تعريفيتين عن الشخصية مع صورة لصاحب او صاحبة الشخصية وفي نهاية المسابقة يمكن للفرد ان يسجل بياناته لاستلام شهادة باجتيازه المسابقة كما ان اسمه سيكون مسجلا بين الفائزين سواء على التطبيق او على الموقع الخاص بالمسابقة www.newakhtha.com  </span></p></li>
        </ul>
        
        
        
        
      </div>
    </div>
    <!-- 2nd row -->
    <div class="row mt-5">
      
      <div class="col-md-6 ml-auto pl-md-6">
       <!--  <span class="text-cursive h5 text-red">مربي
        </span> -->
        <ul>
          <li><p><span>&nbsp;&nbsp;نيمكن استخدام المسابقة ضمن الاطار المدرسي كاحد الادوات لاثراء الحصة التعليمية في اي مادة من المواد او يمكن اعطاء الطلبة واجب مدرسي للاطلاع وجمع معلومات عن بعض الشخصيات التي في المسابقة كما يمكن خلق وادارة حوارات ومناقشات حول الشخصيات مع استعانة الطالب بمعلومات من الاسرة .</span></p></li>
          <li><p><span>&nbsp;&nbsp;نالمسابقة يمكن ان تستخدم كموضوعات بحث او بالاحرى فتح شهية وخلق فضول لدى الطلبة للبحث وفي ذلك اثراء للثقافة العامة وتحقيق للمقولة (اللي ماله اول ماله تالي)</span></p></li>
          <li><p><span>&nbsp;&nbsp;نارجو من الجميع التواصل مباشرة حول اية ملاحظات ومقترحات يمكن الاخذ بها في الاصدار الثاني والذي سيتضمن مجموعة اخرى من الرواد ليصل العدد الى 250 من رواد الخليج قبل نهاية العام 2020.</span></p></li>
        </ul>
        
         
         
        
      </div>
      <div class="col-md-6 mb-5 mb-md-0">
        <img src="images/img_1.jpg" alt="Image" class="img-fluid">
      </div>
    </div>



  </div>
</div>


<!--     <div class="site-section bg-info">
  <div class="container">
    <div class="row mb-5">
      <div class="col-12 text-center">
        <span class="text-cursive h5 text-red d-block">Packages You Like</span>
        <h2 class="text-white">Our Packages</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-4 mb-4 mb-lg-0">
        <div class="package text-center bg-white">
          <span class="img-wrap"><img src="images/flaticon/svg/001-jigsaw.svg" alt="Image" class="img-fluid"></span>
          <h3 class="text-teal">Indoor Games</h3>
          <p>Lorem ipsum dolor sit amet. Consequatur aliquam, fuga maiores amet quo corporis distinctio soluta recusandae?</p>
          <p><a href="#" class="btn btn-primary btn-custom-1 mt-4">Learn More</a></p>
        </div>
      </div>
      <div class="col-lg-4 mb-4 mb-lg-0">
        <div class="package text-center bg-white">
          <span class="img-wrap"><img src="images/flaticon/svg/002-target.svg" alt="Image" class="img-fluid"></span>
          <h3 class="text-success">Outdoor Game and Event</h3>
          <p>Lorem ipsum dolor sit amet. Consequatur aliquam, fuga maiores amet quo corporis distinctio soluta recusandae?</p>
          <p><a href="#" class="btn btn-warning btn-custom-1 mt-4">Learn More</a></p>
        </div>
      </div>
      <div class="col-lg-4 mb-4 mb-lg-0">
        <div class="package text-center bg-white">
          <span class="img-wrap"><img src="images/flaticon/svg/003-mission.svg" alt="Image" class="img-fluid"></span>
          <h3 class="text-danger">Camping for Kids</h3>
          <p>Lorem ipsum dolor sit amet. Consequatur aliquam, fuga maiores amet quo corporis distinctio soluta recusandae?</p>
          <p><a href="#" class="btn btn-success btn-custom-1 mt-4">Learn More</a></p>
        </div>
      </div>
    </div>
  </div>
</div> -->
<!--     <div class="site-section">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <span class="text-cursive h5 text-red d-block">Pricing Plan</span>
        <h2 class="text-black">Our Pricing</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo quis cupiditate fugit, voluptatibus ullam, non laboriosam alias veniam, ex inventore iure sed?</p>
      </div>
      <div class="col-md-4">
        <div class="pricing teal">
          <span class="price">
            <span>$30</span>
          </span>
          <h3>Silver Pack</h3>
          <ul class="ul-check list-unstyled teal">
            <li>Lorem ipsum dolor sit amet</li>
            <li>Consectetur adipisicing elit</li>
            <li>Nemo quis cupiditate</li>
          </ul>
          <p><a href="#" class="btn btn-teal btn-custom-1 mt-4">Buy Now</a></p>
        </div>
      </div>
      <div class="col-md-4">
        <div class="pricing danger">
          <span class="price">
            <span>$70</span>
          </span>
          <h3>Golden Pack</h3>
          <ul class="ul-check list-unstyled danger">
            <li>Lorem ipsum dolor sit amet</li>
            <li>Consectetur adipisicing elit</li>
            <li>Nemo quis cupiditate</li>
          </ul>
          <p><a href="#" class="btn btn-danger btn-custom-1 mt-4">Buy Now</a></p>
        </div>
      </div>
    </div>
  </div>
</div> -->

<!--     <div class="site-section bg-light">
  <div class="container">
    <div class="row mb-5">
      <div class="col-12 text-center">
        <span class="text-cursive h5 text-red d-block">Testimonial</span>
        <h2 class="text-black">What Our Client Says About Us</h2>
      </div>
    </div>
    <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="testimonial-3-wrap">
          
          <div class="owl-carousel nonloop-block-13">
            <div class="testimonial-3 d-flex">
              <div class="vcard-wrap mr-5">
                <img src="images/person_1.jpg" alt="Image" class="vcard img-fluid">
              </div>
              <div class="text">
                <h3>Jeff Woodland</h3>
                <p class="position">Partner</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam earum libero rem maxime magnam. Similique esse ab earum, autem consectetur.</p>
              </div>
            </div>
            <div class="testimonial-3 d-flex">
              <div class="vcard-wrap mr-5">
                <img src="images/person_3.jpg" alt="Image" class="vcard img-fluid">
              </div>
              <div class="text">
                <h3>Jeff Woodland</h3>
                <p class="position">Partner</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam earum libero rem maxime magnam. Similique esse ab earum, autem consectetur.</p>
              </div>
            </div>
            <div class="testimonial-3 d-flex">
              <div class="vcard-wrap mr-5">
                <img src="images/person_2.jpg" alt="Image" class="vcard img-fluid">
              </div>
              <div class="text">
                <h3>Jeff Woodland</h3>
                <p class="position">Partner</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam earum libero rem maxime magnam. Similique esse ab earum, autem consectetur.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row mt-5 justify-content-center">
      <div class="col-md-8">
        
        <div class="row">
          <div class="col-lg-3 text-center">
            <span class="text-teal h2 d-block">3423</span>
            <span>Happy Client</span>
          </div>
          <div class="col-lg-3 text-center">
            <span class="text-yellow h2 d-block">4398</span>
            <span>Members</span>
          </div>
          <div class="col-lg-3 text-center">
            <span class="text-success h2 d-block">50+</span>
            <span>Staffs</span>
          </div>
          <div class="col-lg-3 text-center">
            <span class="text-danger h2 d-block">2000+</span>
            <span>Our Followers</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> -->
<div class="site-section py-5 bg-warning">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12 d-flex">
        <h2 class="text-white m-0">Bring Fun Life To Your Kids</h2>
        <a href="#" class="btn btn-primary btn-custom-1 py-3 px-5 ml-auto">Get Started</a>
      </div>
    </div>
  </div>
</div>

<?php include('includes/main_footer.php'); ?>