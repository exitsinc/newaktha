<?php include('includes/main_header.php'); ?>

    <div class="ftco-blocks-cover-1">
       
      <div class="site-section-cover overlay">
        <div class="container">
          <div class="row align-items-center ">
            <div class="col-lg-6 col-md-6 mt-5 pt-5">
              <!-- <span class="text-cursive h5 text-red">Welcome To Our Website</span> -->
              <h1 class="mb-3 font-weight-bold text-teal">أهلا في موقعنا</h1>
              <!-- <p>Amazing Playground for your kids</p>
              <p class="mt-5"><a href="#" class="btn btn-primary py-4 btn-custom-1">Learn More</a></p> -->
            </div>
            <div class="col-md-6 ml-auto align-self-end mb-5">
              <!-- <img src="images/kid_transparent.png" alt="Image" class="img-fluid"> -->
              <img src="images/old-house-of-arabia-1056759.jpg" alt="Image" class="img-fluid">
              
            </div>
          </div>
        </div>
      </div>
    </div>

   <!--  <div class="site-section">
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <div class="block-2 red">
              <span class="wrap-icon">
                <span class="icon-home"></span>
              </span>
              <h2>Indoor Games</h2>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima nesciunt, mollitia, hic enim id culpa.</p>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="block-2 yellow">
              <span class="wrap-icon">
                <span class="icon-person"></span>
              </span>
              <h2>Outdoor Game And Event</h2>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima nesciunt, mollitia, hic enim id culpa.</p>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="block-2 teal">
              <span class="wrap-icon">
                <span class="icon-cog"></span>
              </span>
              <h2>Camping for Kids</h2>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima nesciunt, mollitia, hic enim id culpa.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
 -->
    <div class="site-section bg-light">
      <div class="container">
        <div class="row">
          <div class="col-md-6 mb-5 mb-md-0">
            <img src="images/arebic.jpeg" alt="Image" class="img-fluid">
          </div>
          <div class="col-md-6 ml-auto pl-md-5">
            <!-- <span class="text-cursive h5 text-red">About Us</span>
            <h3 class="text-black">Bring Fun Life To Your Kids</h3> -->
            <p><span>حياكم الله في موقع نواخذه ....
كلمة نواخذه هي جمع كلمة نوخذه وهي مصطلح كان ومازال مستخدما  في منطقة الخليج العربي وتعني ربان او قبطان السفينة ... ونحن نستخدمها هنا بالمعنى الرمزي لتعني القادة فالنوخذه فعلا هو القائد او الرئيس التنفيذي او المدير العام في السفينة حسب مصطلحات اليوم .
</span></p>
         <p>هذا الموقع مرتبط بمسابقة تحت عنوان ( نواخذه ) وهي مسابقة تعليمية ثقافية تحتوي على 125 سؤال في الاصدار الاول وكل سؤال حول اسم من اسماء رواد الخليج  ، بعضهم قد رحل عنا والبعض الاخر أطال الله في اعمارهم مستمرين في العطاء ... الهدف هو تعريف ابناء هذا الجيل باسماء واعمال العمالقة الكبار الذي بنوا ما نراه اليوم وتأكيدا للمثل الشعبي (اللي ماله اول ماله تالي).
</p>

          <!--   <p class="mt-5"><a href="#" class="btn btn-warning py-4 btn-custom-1">More About Us</a></p> -->
          </div>
        </div>
      </div>
    </div>
    
    
    <div class="site-section bg-info">
      <div class="container">
        <div class="row mb-5">
          <div class="col-12 text-center">
            <!-- <span class="text-cursive h5 text-red d-block">Packages You Like</span> -->
            <h2 class="text-white">قبل الدخول في تفاصيل المسابقة  والاسئلة نود الاشارة الى النقاط التالية 
</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12 mb-4 mb-lg-0">
            <div class="package text-left bg-white">
              <!-- <span class="img-wrap"><img src="images/flaticon/svg/001-jigsaw.svg" alt="Image" class="img-fluid"></span> -->
              <!-- <h3 class="text-teal">Indoor Games</h3> -->
              <ol>
                <li><p>ذا هو الاصدار الاول من المسابقة وسيصدر الاصدار الثاني قبل نهاية العام 2020 ففي هذا الاصدار رصدنا 125  رائدا فقط من الخليج بينما لدينا الكثير من الاسماء التي لم ترد هنا وسيتم اضافتها في النسخة الثانية .</p></li>
                <li><p>رجو من الجميع التواصل معنا بارسال ملاحظاتهم وانطباعتهم عن المسابقة وارسال اية اسماء اخرى يرون انهم تركوا بصمة وانهم يستحقوا الاشارة والاشادة. </p></li>
                <li><p>ود الاعتذار مسبقا من الجميع على عدم جودة بعض الصور من ناحية ونرحب باية صور اخرى اكثر جودة للنسخة الثانية .</p></li>
                <li><p>قد راعينا قدر الامكان البحث عن الصور التي ليس بها قيود حماية حقوق الملكية ونرجو ان نكون قد وفقنا فان لم ننجح في ذلك فاننا نعتذر مسبقا ويسعدنا الاشارة الى حقوق الملكية لاية صورة ترسل لنا .
                </p></li>
                
              </ol>
              
               <p></p>
               <p></p>

              <!-- <p><a href="#" class="btn btn-primary btn-custom-1 mt-4">Learn More</a></p> -->
            </div>
          </div>
          <!-- <div class="col-lg-4 mb-4 mb-lg-0">
            <div class="package text-center bg-white">
              <span class="img-wrap"><img src="images/flaticon/svg/68042-200.png" alt="Image" class="img-fluid"></span>
              <h3 class="text-success">Outdoor Game and Event</h3>
              <p>رجو من الجميع التواصل معنا بارسال ملاحظاتهم وانطباعتهم عن المسابقة وارسال اية اسماء اخرى يرون انهم تركوا بصمة وانهم يستحقوا الاشارة والاشادة. </p>
              <p><a href="#" class="btn btn-warning btn-custom-1 mt-4">Learn More</a></p>
            </div>
          </div> -->
         <!--  <div class="col-lg-4 mb-4 mb-lg-0">
            <div class="package text-center bg-white">
              <span class="img-wrap"><img src="images/flaticon/svg/003-mission.svg" alt="Image" class="img-fluid"></span>
              <h3 class="text-danger">Camping for Kids</h3>
              <p>Lorem ipsum dolor sit amet. Consequatur aliquam, fuga maiores amet quo corporis distinctio soluta recusandae?</p>
              <p><a href="#" class="btn btn-success btn-custom-1 mt-4">Learn More</a></p>
            </div>
          </div> -->
        </div>
      </div>
    </div>

    <div class="site-section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <!-- <span class="text-cursive h5 text-red d-block">Pricing Plan</span> -->
            <h2 class="text-black" style="border: 1px solid #000;
    padding: 22px 15px;
    border-top-right-radius: 50px;
    border-bottom-left-radius: 50px;">أود ان اشكر بعض الاخوة الذين ساهموا في انجاح هذا المشروع بتأكيد بعض المعلومات او البحث عن الصور ومنهم سعادة السفير خليل الذوادي والمؤرخ الرياضي الاستاذ ناصر محمد (بو بدر) والمستشار الاخ عبدالمنعم العيد والاخ الاستاذ خليفه صليبيخ من البحرين والاخ العزيز خالد مهوس والاستاذ زين امين من المملكة العربية السعودية .
</h2>
            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo quis cupiditate fugit, voluptatibus ullam, non laboriosam alias veniam, ex inventore iure sed?</p> -->
          </div>
          
          <!-- <div class="col-md-4">
            <div class="pricing teal">
              <span class="price">
                <span>$30</span>
              </span>
              <h3>Silver Pack</h3>
              <ul class="ul-check list-unstyled teal">
                <li>Lorem ipsum dolor sit amet</li>  
                <li>Consectetur adipisicing elit</li>
                <li>Nemo quis cupiditate</li>
              </ul>
              <p><a href="#" class="btn btn-teal btn-custom-1 mt-4">Buy Now</a></p>
            </div>
          </div> -->
          <!-- <div class="col-md-4">
            <div class="pricing danger">
              <span class="price">
                <span>$70</span>
              </span>
              <h3>Golden Pack</h3>
              <ul class="ul-check list-unstyled danger">
                <li>Lorem ipsum dolor sit amet</li>  
                <li>Consectetur adipisicing elit</li>
                <li>Nemo quis cupiditate</li>
              </ul>
              <p><a href="#" class="btn btn-danger btn-custom-1 mt-4">Buy Now</a></p>
            </div>
          </div> -->
        </div>
        <!-- 2nd row -->
        <div class="row mt-5">
          <div class="col-lg-12 mb-4 mb-lg-0">
            <div class="package text-left bg-dark text-light" style="background-color: #17a2b8!important;">
             <h2 class="text-center mb-4">يمكنكم تنزيل المسابقة بثلاث بدائل 
</h2>
              <ul>
                <li><h3>مكن تنزيل المسابقة هنا من هذا الموقع بالضغط على زر (المسابقة )</h3></li>
                <li><h3>ما يمكن تنزيل المسابقة عن طريق التطبيق على الموبايل اندرويد Google Store </h3></li>
                <li><h3>او عن طريق التطبيق على نظام iphone  من خلال Apple Store </h3></li>
                
              </ul>
              
               <p></p>
               <p></p>

              <!-- <p><a href="#" class="btn btn-primary btn-custom-1 mt-4">Learn More</a></p> -->
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="site-section bg-light">
      <div class="container">
        <div class="row mb-5">
          <div class="col-lg-12 mb-4 mb-lg-0">
            <div class="package text-center bg-white">
              <span class="img-wrap"><img src="images/flaticon/svg/003-mission.svg" alt="Image" class="img-fluid"></span>
              <h3 class="text-danger">من يجتاز المستويات الخمس في الاصدار الاول او العشرة في الاصدار الثاني يحصل على شهادة بذلك يمكن تنزيلها من هذا الموقع او على الجوال كما ان اسماء الفائزين ستكون منشورة في الموقع وعلى التطبيق .</h3>
             <!--  <p>Lorem ipsum dolor sit amet. Consequatur aliquam, fuga maiores amet quo corporis distinctio soluta recusandae?</p> -->
              <!-- <p><a href="#" class="btn btn-success btn-custom-1 mt-4">Learn More</a></p> -->
            </div>
          </div>
          <!-- <div class="col-12 text-center">
            <span class="text-cursive h5 text-red d-block">Testimonial</span>
            <h2 class="text-black">What Our Client Says About Us</h2>
          </div> -->
        </div>
    <!--     <div class="row justify-content-center">
          <div class="col-md-8">
            <div class="testimonial-3-wrap">
              

              <div class="owl-carousel nonloop-block-13">
                <div class="testimonial-3 d-flex">
                  <div class="vcard-wrap mr-5">
                    <img src="images/person_1.jpg" alt="Image" class="vcard img-fluid">
                  </div>
                  <div class="text">
                    <h3>Jeff Woodland</h3>
                    <p class="position">Partner</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam earum libero rem maxime magnam. Similique esse ab earum, autem consectetur.</p>
                  </div>
                </div>

                <div class="testimonial-3 d-flex">
                  <div class="vcard-wrap mr-5">
                    <img src="images/person_3.jpg" alt="Image" class="vcard img-fluid">
                  </div>
                  <div class="text">
                    <h3>Jeff Woodland</h3>
                    <p class="position">Partner</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam earum libero rem maxime magnam. Similique esse ab earum, autem consectetur.</p>
                  </div>
                </div>

                <div class="testimonial-3 d-flex">
                  <div class="vcard-wrap mr-5">
                    <img src="images/person_2.jpg" alt="Image" class="vcard img-fluid">
                  </div>
                  <div class="text">
                    <h3>Jeff Woodland</h3>
                    <p class="position">Partner</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam earum libero rem maxime magnam. Similique esse ab earum, autem consectetur.</p>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div> -->
  <!--       <div class="row mt-5 justify-content-center">

          <div class="col-md-8">
            

            <div class="row">
              <div class="col-lg-3 text-center">
                <span class="text-teal h2 d-block">3423</span>
                <span>Happy Client</span>
              </div>
              <div class="col-lg-3 text-center">
                <span class="text-yellow h2 d-block">4398</span>
                <span>Members</span>
              </div>
              <div class="col-lg-3 text-center">
                <span class="text-success h2 d-block">50+</span>
                <span>Staffs</span>
              </div>
              <div class="col-lg-3 text-center">
                <span class="text-danger h2 d-block">2000+</span>
                <span>Our Followers</span>
              </div>
            </div>

          </div>
        </div> -->
      </div>
    </div>


    <div class="site-section py-5 bg-warning">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-12 d-flex">
          <h2 class="text-white m-0">نرجو التواصل معي مباشرة لاية ملاحظات او اقتراحات اما بتعبئة النموذج الموجود في الصفحات التالية او الكتابة على ايميل info@newakhtha.com   
مع كل الحب والتقدير .... 

</h2>
         <!--  <a href="#" class="btn btn-primary btn-custom-1 py-3 px-5 ml-auto">Get Started</a> -->
          </div>
        </div>
      </div>
    </div>

    

 <?php include('includes/main_footer.php'); ?>